using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblPALM_LOAN {
        public TblPALM_LOAN() { }
        public int LOAN_NO { get; set; }
        public int? PALM_NO { get; set; }
        public int? EXHIB_ID { get; set; }
        public int? KEEP_ID { get; set; }
        public System.DateTime?LOAN { get; set; }
        public System.DateTime?RETURN { get; set; }
        public string COMMENTS { get; set; }
        public string CONT_INFO { get; set; }
    }
}
