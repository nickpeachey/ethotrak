using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class tbldErrorLog {
        public tbldErrorLog() { }
        public int ID { get; set; }
        public int? CustomNumber { get; set; }
        public int? ErrorNumber { get; set; }
        public string ErrorDescription { get; set; }
        public System.DateTime?DateLog { get; set; }
        public int? ACT_TP { get; set; }
        public System.DateTime?ACT_DT { get; set; }
    }
}
