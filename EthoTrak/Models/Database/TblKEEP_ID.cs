using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblKEEP_ID {
        public TblKEEP_ID() { }
        public virtual int KEEP_ID { get; set; }
        public virtual string FIRST { get; set; }
        public virtual string MID_INIT { get; set; }
        public virtual string LAST { get; set; }
        public virtual string KEEP_INIT { get; set; }
        public virtual System.DateTime? ADD_DATE { get; set; }
        public virtual int? ACT_TP { get; set; }
        public virtual System.DateTime? ACT_DT { get; set; }
    }
}
