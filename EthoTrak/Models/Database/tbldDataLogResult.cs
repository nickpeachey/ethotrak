using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class tbldDataLogResult {
        public tbldDataLogResult() { }
        public int? SourceID { get; set; }
        public System.DateTime?DateUploaded { get; set; }
    }
}
