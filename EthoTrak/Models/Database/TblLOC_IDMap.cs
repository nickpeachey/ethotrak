using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblLOC_IDMap : ClassMap<TblLOC_ID> {
        
        public TblLOC_IDMap() {
			Table("TblLOC_ID");
			LazyLoad();
			Id(x => x.LOC_ID).GeneratedBy.Identity().Column("LOC_ID");
			Map(x => x.ENCLOS_ID).Column("ENCLOS_ID");
			Map(x => x.LOC).Column("LOC").Length(50);
			Map(x => x.ADD_DATE).Column("ADD_DATE");
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
        }
    }
}
