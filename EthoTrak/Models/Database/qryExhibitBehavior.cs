using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class qryExhibitBehavior {
        public qryExhibitBehavior() { }
        public int Exhib_ID { get; set; }
        public int BEHAV_ID { get; set; }
        public int? MSTR_ID { get; set; }
        public int? SPEC_ID { get; set; }
        public string BEHAV { get; set; }
        public string DEFIN { get; set; }
        public System.Nullable<bool> ALL_OCC { get; set; }
        public System.DateTime?ADD_DATE { get; set; }
        public int? BEH_PRNT { get; set; }
        public System.Nullable<bool> Locked { get; set; }
        public int? ACT_TP { get; set; }
        public System.DateTime?ACT_DT { get; set; }
    }
}
