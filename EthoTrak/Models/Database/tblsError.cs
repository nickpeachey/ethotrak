using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class tblsError {
        public tblsError() { }
        public int ID { get; set; }
        public int? ErrorNumber { get; set; }
        public string ErrorDescripton { get; set; }
        public string ErrorMessage { get; set; }
        public System.DateTime?DateAdded { get; set; }
        public System.DateTime?DateUpdated { get; set; }
    }
}
