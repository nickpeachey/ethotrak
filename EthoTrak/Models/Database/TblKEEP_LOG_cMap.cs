using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;

namespace EthoTrak.DatabaseModels {
    
    
    public class TblKEEP_LOG_cMap : ClassMap<TblKEEP_LOG_c> {
        
        public TblKEEP_LOG_cMap() {
			Table("TblKEEP_LOG_c");
			LazyLoad();
			CompositeId().KeyProperty(x => x.USER_ID, "USER_ID").KeyProperty(x => x.SES_NO, "SES_NO");
			Map(x => x.ID).Column("ID").Not.Nullable();
			Map(x => x.KEEP_ID).Column("KEEP_ID");
			Map(x => x.AREA_ID).Column("AREA_ID");
			Map(x => x.SKY).Column("SKY").Length(10);
			Map(x => x.TEMP_LOG).Column("TEMP_LOG");
			Map(x => x.HUMI_LOG).Column("HUMI_LOG");
			Map(x => x.CROWD).Column("CROWD").Length(10);
			Map(x => x.DATE_IN).Column("DATE_IN");
			Map(x => x.TIME_IN).Column("TIME_IN");
			Map(x => x.DATE_OUT).Column("DATE_OUT");
			Map(x => x.TIME_OUT).Column("TIME_OUT");
			Map(x => x.SES_SC).Column("SES_SC");
			Map(x => x.ACT_TP).Column("ACT_TP");
			Map(x => x.ACT_DT).Column("ACT_DT");
			Map(x => x.TYPE).Column("TYPE");
			Map(x => x.COMMENTS).Column("COMMENTS").Length(255);
        }
    }
}
