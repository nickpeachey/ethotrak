using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblNEIGH_OBS_c {
        public TblNEIGH_OBS_c() { }
        public string USER_ID { get; set; }
        public int SES_NO { get; set; }
        public int OBS_NO { get; set; }
        public int NE_ORD { get; set; }
        public int ID { get; set; }
        public int? ID_OBS { get; set; }
        public int? SPEC_ID { get; set; }
        public int? NE_OBS { get; set; }
        public string RELAT { get; set; }
        public int? DIST { get; set; }
        public int? ACT_TP { get; set; }
        public System.DateTime?ACT_DT { get; set; }
    }
}
