using System;
using System.Text;
using System.Collections.Generic;


namespace EthoTrak.DatabaseModels {
    
    public class TblDATA_TYPE {
        public TblDATA_TYPE() { }
        public int ID { get; set; }
        public int? TYPE_ID { get; set; }
        public string TYPE_NAME { get; set; }
        public string TYPE_DEF { get; set; }
    }
}
