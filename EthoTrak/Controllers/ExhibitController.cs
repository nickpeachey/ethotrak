﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    public class ExhibitController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Add()
        {
            var exhibit = new TblEXHIB_ID();
            return View(exhibit);
        }

        [HttpPost]
        public ActionResult Add(TblEXHIB_ID exhibit)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    exhibit.ADD_DATE = System.DateTime.Now;
                    exhibit.ACT_DT = exhibit.ADD_DATE;

                    session.Save(exhibit);

                    transaction.Commit();
                }
            }

            return Redirect("/Exhibit/All");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var exhibit = session.QueryOver<TblEXHIB_ID>().Where(x => x.EXHIB_ID == id).SingleOrDefault();

                    if (exhibit == null)
                    {
                        return HttpNotFound();
                    }

                    return View(exhibit);
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, TblEXHIB_ID exhibit)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbExhibit = session.QueryOver<TblEXHIB_ID>().Where(x => x.EXHIB_ID == id).SingleOrDefault();

                    dbExhibit.EXHIB = exhibit.EXHIB;
                    dbExhibit.ACT_DT = System.DateTime.Now;
                    
                    session.Save(dbExhibit);

                    transaction.Commit();
                }
            }
            return Redirect("/Exhibit/All");
        }

        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.exhibits = session.QueryOver<TblEXHIB_ID>().List().OrderBy(x => x.EXHIB_ID);
                }
            }

            return View();
        }

        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbAreas = session.QueryOver<TblAREA_ID>().Where(x => x.EXHIB_ID == id).List();

                    foreach (var area in dbAreas) 
                    {
                        EthoTrak.Controllers.AreaController ac = new AreaController();
                        ac.Delete(area.AREA_ID);
                    }

                    var dbExhibit = session.QueryOver<TblEXHIB_ID>().Where(x => x.EXHIB_ID == id).SingleOrDefault();
                    session.Delete(dbExhibit);
                    transaction.Commit();
                }
            }
            return Redirect("/Exhibit/All");
        }
    }
}
