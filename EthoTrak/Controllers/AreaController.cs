﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    public class AreaController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Add()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var exhibits = session.QueryOver<TblEXHIB_ID>().List();

                    IEnumerable<SelectListItem> list = exhibits.Select(exhibit => new SelectListItem
                    {
                        Value = exhibit.EXHIB_ID.ToString(),
                        Text = exhibit.EXHIB
                    });

                    ViewBag.exhibits = list;
                }
            }

            var area = new TblAREA_ID();
            return View(area);
        }

        [HttpPost]
        public ActionResult Add(TblAREA_ID a)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    a.ACT_DT = System.DateTime.Now;

                    session.Save(a);

                    transaction.Commit();
                }
            }

            return Redirect("/Area/All");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var area = session.QueryOver<TblAREA_ID>().Where(x => x.AREA_ID == id).SingleOrDefault();

                    if (area == null)
                    {
                        return HttpNotFound();
                    }

                    var exhibits = session.QueryOver<TblEXHIB_ID>().List();

                    IEnumerable<SelectListItem> list = exhibits.Select(exhibit => new SelectListItem
                    {
                        Value = exhibit.EXHIB_ID.ToString(),
                        Text = exhibit.EXHIB
                    });

                    ViewBag.exhibits = list;

                    return View(area);
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, TblAREA_ID a)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbArea = session.QueryOver<TblAREA_ID>().Where(x => x.AREA_ID == id).SingleOrDefault();

                    dbArea.AREA = a.AREA;
                    dbArea.EXHIB_ID = a.EXHIB_ID;
                    dbArea.ACT_DT = System.DateTime.Now;

                    session.Save(dbArea);

                    transaction.Commit();
                }
            }
            return Redirect("/Area/All");
        }

        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.areas = session.QueryOver<TblAREA_ID>().List().OrderBy(x => x.AREA_ID);
                }
            }

            return View();
        }

        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbEnclosures = session.QueryOver<TblENCLOS_ID>().Where(x => x.AREA_ID == id).List();

                    foreach (var enclosure in dbEnclosures)
                    {
                        EthoTrak.Controllers.EnclosureController ec = new EnclosureController();
                        ec.Delete(enclosure.ENCLOS_ID);
                    }
                    
                    var dbArea = session.QueryOver<TblAREA_ID>().Where(x => x.AREA_ID == id).SingleOrDefault();
                    session.Delete(dbArea);
                    transaction.Commit();
                }
            }
            return Redirect("/Area/All");
        }
    }
}
