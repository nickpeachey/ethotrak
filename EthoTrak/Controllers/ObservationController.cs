﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    public class ObservationController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        /*
        [HttpGet]
        public ActionResult Add()
        {
            var observations = new TblANIM_OBS_c();
            return View(observations);
        }

        [HttpPost]
        public ActionResult Add(TblANIM_OBS_c observation)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    observation.ACT_DT = System.DateTime.Now;
                    session.Save(observation);
                    transaction.Commit();
                }
            }
            return Redirect("/Observation/All");
        }
        */
        
        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var observation = session.QueryOver<TblANIM_OBS_c>().Where(x => x.ID == id).SingleOrDefault();

                    if (observation == null)
                    {
                        return HttpNotFound();
                    }

                    var species = session.QueryOver<TblSPEC_ID>().List();

                    IEnumerable<SelectListItem> list = species.Select(s => new SelectListItem
                    {
                        Value = s.SPEC_ID.ToString(),
                        Text = s.SPEC_NAME
                    });

                    ViewBag.species = list;

                    var animals = session.QueryOver<TblANIM_ID>().Where(x => x.SPEC_ID == observation.SPEC_ID).List();

                    list = animals.Select(a => new SelectListItem
                    {
                        Value = a.ANIM_ID.ToString(),
                        Text = a.DESC_ANIM
                    });

                    ViewBag.animals = list;

                    TblANIM_ID currentAnimal = session.QueryOver<TblANIM_ID>().Where(x => x.ANIM_ID == observation.ID_OBS).SingleOrDefault();
                    if (!animals.Contains(currentAnimal))
                    {
                        observation.ID_OBS = animals.First().ANIM_ID;
                    }

                    var enclosuresIdsAssignedToThisAnimal = session.QueryOver<TblANIM_ENCLO>().Where(x => x.ANIM_ID == observation.ID_OBS).Select(x => x.ENCLOS_ID).List<int>().ToArray();

                    var enclosuresAssignedToThisAnimal = session.QueryOver<TblENCLOS_ID>().WhereRestrictionOn(x => x.ENCLOS_ID).IsIn(enclosuresIdsAssignedToThisAnimal).List();

                    list = enclosuresAssignedToThisAnimal.Select(e => new SelectListItem
                    {
                        Value = e.ENCLOS_ID.ToString(),
                        Text = e.ENCLOS
                    });

                    ViewBag.enclosures = list;

                    TblENCLOS_ID currentEnclosure = session.QueryOver<TblENCLOS_ID>().Where(x => x.ENCLOS_ID == observation.ENCLOS_ID).SingleOrDefault();
                    if (!enclosuresAssignedToThisAnimal.Contains(currentEnclosure))
                    {
                        observation.ENCLOS_ID = enclosuresAssignedToThisAnimal.First().ENCLOS_ID;
                    }


                    //ADD BEHAVIOR
                    System.Diagnostics.Debug.WriteLine(observation.ENCLOS_ID);

                    var locations = session.QueryOver<TblLOC_ID>().Where(x => x.ENCLOS_ID == observation.ENCLOS_ID).List();

                    list = locations.Select(l => new SelectListItem
                    {
                        Value = l.LOC_ID.ToString(),
                        Text = l.LOC
                    });

                    ViewBag.locations = list;

                    ViewBag.Date = observation.DATE_OBS.Value.ToShortDateString();

                    return View(observation);
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, TblANIM_OBS_c observation, int refresh)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbObservation = session.QueryOver<TblANIM_OBS_c>().Where(x => x.ID == id).SingleOrDefault();

                    dbObservation.SPEC_ID = observation.SPEC_ID;
                    dbObservation.ID_OBS = observation.ID_OBS;
                    dbObservation.ENCLOS_ID = observation.ENCLOS_ID;
                    dbObservation.BEHAV_ID = observation.BEHAV_ID;
                    dbObservation.LOC_ID = observation.LOC_ID;
                    dbObservation.DATE_OBS = observation.DATE_OBS;
                    dbObservation.FLAG = observation.FLAG;
                    dbObservation.ACT_DT = System.DateTime.Now;

                    session.Save(dbObservation);

                    transaction.Commit();
                }
            }
            if (refresh == 1)
                return Edit(observation.ID);
            
            return Redirect("/Observation/All");
        }

        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var observations = session.QueryOver<TblANIM_OBS_c>().List().OrderBy(x => x.ID);

                    var list = new List<TblANIM_OBS_c_ViewModel>();

                    foreach (var observation in observations)
                    {
                        var obs = new TblANIM_OBS_c_ViewModel();

                        System.Diagnostics.Debug.WriteLine("obs ID : " + observation.SPEC_ID);

                        obs.ID = observation.ID;
                        obs.USER_ID = observation.USER_ID;
                        obs.SES_NO = observation.SES_NO;
                        obs.OBS_NO = observation.OBS_NO;

                        if (session.QueryOver<TblSPEC_ID>().Where(x => x.SPEC_ID == observation.SPEC_ID).RowCount() != 0)
                            obs.SPEC = session.QueryOver<TblSPEC_ID>().Where(x => x.SPEC_ID == observation.SPEC_ID).SingleOrDefault().SPEC_NAME;
                        else
                            obs.SPEC = "deleted";

                        if (session.QueryOver<TblANIM_ID>().Where(x => x.ANIM_ID == observation.ID_OBS).RowCount() != 0)
                            obs.ANIMAL = session.QueryOver<TblANIM_ID>().Where(x => x.ANIM_ID == observation.ID_OBS).SingleOrDefault().DESC_ANIM;
                        else
                            obs.ANIMAL = "deleted";

                        if (session.QueryOver<TblENCLOS_ID>().Where(x => x.ENCLOS_ID == observation.ENCLOS_ID).RowCount() != 0)
                            obs.ENCLOSURE = session.QueryOver<TblENCLOS_ID>().Where(x => x.ENCLOS_ID == observation.ENCLOS_ID).SingleOrDefault().ENCLOS;
                        else
                            obs.ENCLOSURE = "deleted";

                        if (session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == observation.BEHAV_ID).RowCount() != 0)
                            obs.BEHAVIOR = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == observation.BEHAV_ID).SingleOrDefault().BEHAV;
                        else
                            obs.ENCLOSURE = "deleted";

                        if (session.QueryOver<TblLOC_ID>().Where(x => x.LOC_ID == observation.LOC_ID).RowCount() != 0)
                            obs.LOCATION = session.QueryOver<TblLOC_ID>().Where(x => x.LOC_ID == observation.LOC_ID).SingleOrDefault().LOC;
                        else
                            obs.LOCATION = "deleted";

                        obs.DATE_OBS = observation.DATE_OBS;
                        obs.COMMENTS = observation.COMMENTS;
                        obs.FLAG = observation.FLAG;
                        obs.ACT_DT = observation.ACT_DT;

                        list.Add(obs);
                    }
                    ViewBag.observations = list;
                }
            }

            return View();
        }			

        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbObservation = session.QueryOver<TblANIM_OBS_c>().Where(x => x.ID == id).SingleOrDefault();
                    session.Delete(dbObservation);
                    transaction.Commit();
                }
            }
            return Redirect("/Observation/All");
        }
    }
}
