﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EthoTrak.DatabaseModels;

namespace EthoTrak.Controllers
{
    public class BehaviorController : Controller
    {

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult Add()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var species = session.QueryOver<TblSPEC_ID>().List();

                    IEnumerable<SelectListItem> list = species.Select(s => new SelectListItem
                    {
                        Value = s.SPEC_ID.ToString(),
                        Text = s.SPEC_NAME
                    });

                    ViewBag.species = list;
                }
            }
            var behavior = new TblBEHAV_ID();
            return View(behavior);
        }
        
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult Add(TblBEHAV_ID behavior)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    behavior.ADD_DATE = System.DateTime.Now;
                    behavior.ACT_DT = behavior.ADD_DATE;

                    session.Save(behavior);

                    transaction.Commit();
                }
            }

            return Redirect("/Behavior/All");
        }
        

        
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var behavior = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == id).SingleOrDefault();

                    if (behavior == null)
                    {
                        return HttpNotFound();
                    }

                    var species = session.QueryOver<TblSPEC_ID>().List();

                    IEnumerable<SelectListItem> list = species.Select(s => new SelectListItem
                    {
                        Value = s.SPEC_ID.ToString(),
                        Text = s.SPEC_NAME
                    });

                    ViewBag.species = list;

                    return View(behavior);
                }
            }
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id, TblBEHAV_ID behavior)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbBehavior = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == id).SingleOrDefault();

                    dbBehavior.MSTR_ID = behavior.MSTR_ID;
                    dbBehavior.SPEC_ID = behavior.SPEC_ID;
                    dbBehavior.BEHAV = behavior.BEHAV;
                    dbBehavior.DEFIN = behavior.DEFIN;
                    dbBehavior.BEH_PRNT = behavior.BEH_PRNT;
                    dbBehavior.ACT_DT = System.DateTime.Now;

                    session.Save(dbBehavior);

                    transaction.Commit();
                }
            }
            return Redirect("/Behavior/All");
        }        

        public ActionResult PickSpecies()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var species = session.QueryOver<TblSPEC_ID>().List();

                    IEnumerable<SelectListItem> list = species.Select(s => new SelectListItem
                    {
                        Value = s.SPEC_ID.ToString(),
                        Text = s.SPEC_NAME
                    });

                    ViewBag.species = list;
                }
            }
            return View();
        }

        public ActionResult PickHOB(int idSpecies, int idHOB, int refresh)
        {


            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var HOBs = session.QueryOver<TblBEHAV_ID>().Where(x => x.SPEC_ID == idSpecies).And(x => x.BEH_PRNT == 0).List();

                    IEnumerable<SelectListItem> list = HOBs.Select(HOB => new SelectListItem
                    {
                        Value = HOB.BEHAV_ID.ToString(),
                        Text = HOB.BEHAV
                    });


                    if (refresh == 1)
                    {
                        ViewBag.description = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == idHOB).SingleOrDefault().DEFIN;
                        ViewBag.HOB = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == idHOB).SingleOrDefault().BEHAV_ID;
                    }
                    else
                    {
                        int first = Convert.ToInt32(list.First().Value);
                        ViewBag.description = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == first).SingleOrDefault().DEFIN;
                        ViewBag.HOB = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == first).SingleOrDefault().BEHAV_ID;
                    }

                    ViewBag.idSpecies = idSpecies;
                    ViewBag.HOBs = list;
                }
            }

            return View();
        }
        
        public ActionResult PickSSB(int idSpecies, int idHOB, int idSSB, int refresh)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var SSBs = session.QueryOver<TblBEHAV_ID>().Where(x => x.SPEC_ID == idSpecies).And(x => x.BEH_PRNT == idHOB).List();

                    IEnumerable<SelectListItem> list = SSBs.Select(SSB => new SelectListItem
                    {
                        Value = SSB.BEHAV_ID.ToString(),
                        Text = SSB.BEHAV
                    });


                    if (refresh == 1)
                    {
                        System.Diagnostics.Debug.WriteLine(idSSB);
                        ViewBag.description = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == idSSB).SingleOrDefault().DEFIN;
                        ViewBag.HOB = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == idSSB).SingleOrDefault().BEHAV_ID;
                    }
                    else
                    {
                        int first = Convert.ToInt32(list.First().Value);
                        ViewBag.description = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == first).SingleOrDefault().DEFIN;
                        ViewBag.HOB = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == first).SingleOrDefault().BEHAV_ID;
                    }

                    ViewBag.idSpecies = idSpecies;
                    ViewBag.idHOB = idHOB;
                    ViewBag.SSBs = list;
                }
            }

            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult All()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ViewBag.behaviors = session.QueryOver<TblBEHAV_ID>().List().OrderBy(x => x.BEHAV_ID);
                }
            }

            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var dbBehavior = session.QueryOver<TblBEHAV_ID>().Where(x => x.BEHAV_ID == id).SingleOrDefault();
                    session.Delete(dbBehavior);
                    transaction.Commit();
                }
            }
            return Redirect("/Behavior/All");
        }
        
    }
}
